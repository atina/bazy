package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "threads", schema = "KunderaExamples@cassandra_pu")
public class ForumThread {

	@Id
	@Column
	private int threadId;

	@Column
	private String title;

	@Column
	private String dateCreated;

	@ManyToOne
	@JoinColumn(name = "user")
	private User user;

	public int getThreadId() {
		return this.threadId;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDateCreated() {
		return this.dateCreated;
	}

	public User getUser() {
		return this.user;
	}

	public void setThreadId(int threadId) {
		this.threadId = threadId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
