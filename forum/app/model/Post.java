package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "posts", schema = "KunderaExamples@cassandra_pu")
public class Post {

	@Id
	@Column
	private int postId;

	@ManyToOne
	@JoinColumn(name = "thread")
	private ForumThread thread;

	@Column
	private String message;

	@Column
	private String date;

	@ManyToOne
	@JoinColumn(name = "user")
	private User user;

	@Column
	private String localization;

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public void setThread(ForumThread thread2) {
		this.thread = thread2;
		//thread.getPosts().add(this);
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setUser(User user) {
		this.user = user;
		//user.getPosts().add(this);
	}

	public void setLocalization(String localization) {
		this.localization = localization;
	}

	public int getPostId() {
		return this.postId;
	}

	public ForumThread getThread() {
		return this.thread;
	}

	public String getMessage() {
		return this.message;
	}

	public String getDate() {
		return this.date;
	}

	public User getUser() {
		return this.user;
	}

	public String getLocalization() {
		return this.localization;
	}

}
