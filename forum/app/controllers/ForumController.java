package controllers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.Persistence;
import model.ForumThread;
import model.Post;
import model.User;
import play.mvc.Controller;
import play.mvc.Result;

public class ForumController extends Controller {

	static EntityManagerFactory emf;

	
	public static Result persist() {
		EntityManager em = getEmf().createEntityManager();
		em.setFlushMode(FlushModeType.COMMIT);
		EntityTransaction tx = em.getTransaction();
		
		String message = null;
		
		try {
			tx.begin();
	
			User user = new User();
			user.setLogin("alan");
			user.setCity("London");
			user.setDateCreated("10/01/2014");
			user.setAge(19);
			user.setSex("M");
			
			em.persist(user);
	
			ForumThread thread = new ForumThread();
			thread.setThreadId(1);
			thread.setTitle("welcome");
			thread.setUser(user);
			thread.setDateCreated("10/01/2014");
			
			em.persist(thread);
	
			Post post = new Post();
			post.setPostId(1);
			post.setThread(thread);
			post.setMessage("Hello everyone, i'm alan");
			post.setUser(user);
			post.setDate("11/01/2014");
			post.setLocalization("Chicago");
			
			em.persist(post);

			tx.commit();
			
		} catch (Exception e) {
				try {
					message = "Changes rollbacked - no data persisted.";
					if((tx != null) && (tx.isActive())) {
						tx.rollback();
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
		}
		
		if(message == null) {
			message = "User, thread and post records persisted.";
		}

		em.close();
		return ok(message);
	}

	public static Result find() {
		EntityManager em = getEmf().createEntityManager();

		User user = em.find(User.class, "alan");
		ForumThread thread = em.find(ForumThread.class, 1);
		Post post = em.find(Post.class, 1);

		em.close();
		return ok("Found records in database with the following details:" + printUser(user)
				+ printThread(thread) + printPost(post));
	}

	
	public static Result update() {
		EntityManager em = getEmf().createEntityManager();
		em.setFlushMode(FlushModeType.COMMIT);
		EntityTransaction tx = em.getTransaction();
		
		User user = null;
		ForumThread thread = null;
		Post post = null;
		String message = null;
		
		try {
			
			tx.begin();
	
			user = em.find(User.class, "alan");
			user.setCity("New York");
			
			em.merge(user);
			
			thread = em.find(ForumThread.class, 1);
			thread.setTitle("welcome again");
	
			em.merge(thread);
	
			post = em.find(Post.class, 1);
			post.setLocalization("Cracow");
	
			em.merge(post);
			tx.commit();
		
		} catch (Exception e) {
			try {
				message = "Changes rollbacked: ";
				if((tx != null) && (tx.isActive())) {
					tx.rollback();
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		em.clear();
		
		user = em.find(User.class, "alan");
		thread = em.find(ForumThread.class, 1);
		post = em.find(Post.class, 1);
		
		if (message == null) {
			message = "Records updated:";
		}

		em.close();
		return ok(message + printUser(user) + printThread(thread) + printPost(post));
	}

	
	public static Result delete() {
		EntityManager em = getEmf().createEntityManager();
		em.setFlushMode(FlushModeType.COMMIT);
		EntityTransaction tx = em.getTransaction();
		
		User user = null;
		ForumThread thread = null;
		Post post = null;
		String message = null;
		
		try {
			tx.begin();
			
			user = em.find(User.class, "alan");
			em.remove(user);
			
			thread = em.find(ForumThread.class, 1);
			em.remove(thread);
			
			post = em.find(Post.class, 1);
			em.remove(post);
			
			tx.commit();
			
		} catch (Exception e) {
			try {
				message = "Changes rollbacked: ";
				if((tx != null) && (tx.isActive())) {
					tx.rollback();
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		em.clear();

		user = em.find(User.class, "alan");
		thread = em.find(ForumThread.class, 1);
		post = em.find(Post.class, 1);
		
		if(message == null) {
			message = "Records deleted : ";
		}
		em.close();
		return ok(message + "\nuser: " + printUser(user) + "\nthread: " + printThread(thread)
				+ "\npost: " + printPost(post));
	}

	private static EntityManagerFactory getEmf() {

		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("cassandra_pu");
		}
		return emf;
	}

	private static String printUser(User user) {

		if (user == null) {
			return "\n--------------------------------------------------\nRecord not found";
		}
		return "\n--------------------------------------------------" + "\nlogin:"
				+ user.getLogin() + "\ncity:" + user.getCity() + "\ndate created:"
				+ user.getDateCreated() + "\nage:" + user.getAge() + "\nsex:" + user.getSex();
	}

	private static String printPost(Post post) {

		if (post == null) {
			return "\n--------------------------------------------------\nRecord not found";
		}
		return "\n--------------------------------------------------" + "\npost:"
				+ post.getPostId() + "\nthread:" + post.getThread().getThreadId() + "\nmessage:"
				+ post.getMessage() + "\ndate:" + post.getDate() + "\nuser:"
				+ post.getUser().getLogin() + "\nlocalization:" + post.getLocalization();
	}

	private static String printThread(ForumThread thread) {

		if (thread == null) {
			return "\n--------------------------------------------------\nRecord not found";
		}
		return "\n--------------------------------------------------" + "\nthread id:"
				+ thread.getThreadId() + "\ntitle:" + thread.getTitle() + "\ndate created:"
				+ thread.getDateCreated() + "\nuser:" + thread.getUser().getLogin();
	}

}
